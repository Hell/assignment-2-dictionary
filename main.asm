%include "words.inc"
%include "lib.inc"

%define buffer_size 255
%define pointer_size 8

section .rodata
not_found_msg: db "Key not found", 0
key_too_long_msg: db "Key is too long (>255)", 0

section .bss
input_buffer resb 256

section .text
global _start

extern find_word

_start:
    mov rdi, input_buffer
    mov rsi, buffer_size
    call read_word

    test rax, rax
    jz .key_too_long

    mov rdi, rax
    mov rsi, elem
    call find_word

    test rax, rax
    jz .key_not_found

    mov rdi, rax
    add rdi, pointer_size
    push rdi
    call string_length

    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string

    xor rdi, rdi
    call exit

.key_too_long:
    mov rdi, key_too_long_msg
    jmp .print_exit

.key_not_found:
    mov rdi, not_found_msg

.print_exit:
    call print_error
    call exit
