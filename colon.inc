%define elem 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
                dq elem
                db %1, 0
        %else
            %error "ID Value needs id value"
        %endif
    %else   
        %error "String value needs string value"
    %endif
    %define elem %2
%endmacro
