%include "lib.inc"
%define pointer_size 8
%define isfound 1
section .text
global find_word

find_word:
    push r12
    push r13
    .loop:
        mov r12, rdi
        mov r13, rsi

        add rsi, pointer_size
        call string_equals

        mov rsi, r13
        mov rdi, r12

        cmp rax, isfound
        je .found

        mov rsi, [rsi]
        test rsi, rsi
        jz .not_found

        jmp .loop

    .found:
        mov rax, rsi
        jmp .pop_ret

    .not_found:
        xor rax, rax

    .pop_ret:
        pop r13
        pop r12
        ret

