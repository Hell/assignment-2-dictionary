import subprocess


tests = [
    {
        'input': 'first',
        'expected_output': 'test1',
        'expected_error': '',
    },
    {
        'input': 'second',
        'expected_output': 'test2',
        'expected_error': '',
    },
    {
        'input': 'third',
        'expected_output': 'test3',
        'expected_error': '',
    },
    {
        'input': 'fourth',
        'expected_output': 'test 4',
        'expected_error': '',
    },
    {
        'input': 'dfssa',
        'expected_output': '',
        'expected_error': 'Key not found',
    },
    {
        'input': 'hi' * 128,
        'expected_output': '',
        'expected_error': 'Key is too long (>255)',
    },
]

failed = 0
count = 0
for test in tests:
    subpr = subprocess.run("./program", input=test['input'], text=True, capture_output=True)
    count += 1

    if (subpr.stdout != test['expected_output'] or subpr.stderr != test['expected_error']):
        print("Test %d failed!" % count)
        print("Input:", test['input'])
        if (subpr.stdout != test['expected_output']):
            print("Expected Output:", test['expected_output'])
            print("Actual Output:", subpr.stdout)
        if (subpr.stderr != test['expected_error']):
            print("Expected Error:", test['expected_error'])
            print("Actual Error:", subpr.stderr)
        print()
        failed += 1

print("================================")
if (failed):
    print("Failed %d tests" % failed)
else:
    print("All tests passed!")
