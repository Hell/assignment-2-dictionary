global exit
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global print_string


section .text

%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define STDERR 2
%define STDOUT 1
%define STDIN 0

%define TAB `\t`
%define NEWLINE `\n`
%define SPACE ` `

%macro CMP_JE 2
    cmp rax, %1
    je %2
%endmacro



; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax  ; Обнуляем rax
.loop:
    cmp byte [rdi+rax], 0  ; Сравниваем текущий байт с нулём (конец строки)
    je .done  ; Если это конец строки, завершаем
    inc rax  ; Увеличиваем счётчик длины
    jmp .loop
.done:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov r10, STDOUT
print:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    mov rdi, r10
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rdi, STDOUT
    mov rax, WRITE_SYSCALL
    syscall
    pop rdi
    ret

    
print_int:
    test rdi,rdi
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi


    
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rdx, rdx
    mov rcx, rsp
    dec rsp
    mov byte[rsp],dl
    mov r9, 10
.loopdiv: 
    xor rdx, rdx                
    div r9
    add rdx, '0'
    dec rsp                 
    mov byte[rsp],dl
    test rax, rax
    ja .loopdiv
    mov rdi,rsp
    push rcx
    call print_string
    pop rsp
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov r8b, byte[rdi+rdx]
    cmp r8b, byte[rsi+rdx]
    jne .ret
    inc rdx
    test r8b, r8b
    jne .loop
    mov rax, 1
.ret:
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
.checkspaces:
    call read_char
    CMP_JE SPACE, .checkspaces
    CMP_JE TAB, .checkspaces
    CMP_JE NEWLINE, .checkspaces
    xor r14, r14
.loop:
    test rax, rax
    je .done
    CMP_JE SPACE, .done
    CMP_JE TAB, .done
    CMP_JE NEWLINE, .done
    cmp r13, r14
    je .error
    mov byte[r12+r14], al
    inc r14
    call read_char
    jmp .loop
.done:
    mov byte[r12+r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .ret
.error:
    xor rax, rax
.ret:
    pop r14
    pop r13
    pop r12
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx

.next_digit:
    mov cl, byte [rdi]
    test rcx, rcx
    jz .done

    cmp rcx, '0'
    jb .done

    cmp rcx, '9'
    ja .done

    sub rcx, '0'
    imul rax, rax, 10
    add rax, rcx

    inc rdi
    inc rdx
    jmp .next_digit

.done:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '+'
    je .plus
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
.plus:
    inc rdi
    call parse_uint
    inc rdx
    ret
.minus:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    jge .error
    mov r8b, byte [rdi+rax]
    mov byte [rsi+rax], r8b
    test r8b, r8b
    inc rax
    jne .loop
    ret
.error:
    xor rax, rax
    ret

print_error:
    mov r10, STDERR
    jmp print
